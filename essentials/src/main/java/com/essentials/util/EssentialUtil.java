package com.essentials.util;

import java.util.Calendar;

import org.springframework.util.StringUtils;

import com.essentials.constant.DayTimeIntervalEnum;
import com.essentials.model.Pin;
import com.essentials.model.UnPin;

public class EssentialUtil {
	public static DayTimeIntervalEnum dayTime(){
		Calendar c = Calendar.getInstance();
		int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
		String dayTime = null;
		
		if(timeOfDay >= 4 && timeOfDay < 12){
			dayTime = "Morning";     
		}else if(timeOfDay >= 12 && timeOfDay < 20){
			dayTime = "Evening";
		}else if(timeOfDay >= 20 || timeOfDay < 4){
			dayTime = "Night";
		}
		
		return DayTimeIntervalEnum.fromValue(dayTime);
	}
	
	public static Pin setDayTimeForPin(Pin pin){
		DayTimeIntervalEnum dayTimeIntervalEnum = dayTime();
		
		switch (dayTimeIntervalEnum) {
		case MORNING:
			pin.setMorning(true);
			break;
			
		case AFTERNOON:
			break;
			
		case EVEVING:
			pin.setEvening(true);
			break;
		case NIGHT:
			pin.setNight(true);
			break;
		default:
			break;
		}
		return pin;
	}
	
	public static UnPin setDayTimeForUnPin(UnPin unpin){
		DayTimeIntervalEnum dayTimeIntervalEnum = dayTime();
		
		switch (dayTimeIntervalEnum) {
		case MORNING:
			unpin.setMorning(true);
			break;
			
		case AFTERNOON:
			break;
			
		case EVEVING:
			unpin.setEvening(true);
			break;
		case NIGHT:
			unpin.setNight(true);
			break;
		default:
			break;
		}
		return unpin;
	}
	
	public static Pin setDayForPin(Pin pin){
		Calendar c = Calendar.getInstance();
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		switch(dayOfWeek){
		case Calendar.SUNDAY:
			pin.setSunday(true);
			break;
			
		case Calendar.MONDAY:
			pin.setMonday(true);
			break;
			
		case Calendar.TUESDAY:
			pin.setTuesday(true);
			break;
			
		case Calendar.WEDNESDAY:
			pin.setWedenesday(true);
			break;
			
		case Calendar.THURSDAY:
			pin.setThrusday(true);
			break;
			
		case Calendar.FRIDAY:
			pin.setFriday(true);
			break;
			
		case Calendar.SATURDAY:
			pin.setSaturday(true);
			break;
		}
		
		return pin;
	}
	
	public static UnPin setDayForUnPin(UnPin unpin){
		Calendar c = Calendar.getInstance();
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		switch(dayOfWeek){
		case Calendar.SUNDAY:
			unpin.setSunday(true);
			break;
			
		case Calendar.MONDAY:
			unpin.setMonday(true);
			break;
			
		case Calendar.TUESDAY:
			unpin.setTuesday(true);
			break;
			
		case Calendar.WEDNESDAY:
			unpin.setWedenesday(true);
			break;
			
		case Calendar.THURSDAY:
			unpin.setThrusday(true);
			break;
			
		case Calendar.FRIDAY:
			unpin.setFriday(true);
			break;
			
		case Calendar.SATURDAY:
			unpin.setSaturday(true);
			break;
		}
		
		return unpin;
	}
	
	public static void validateForNullAndEmpty(String fieldName, String fieldValue) throws Exception{
		if(!StringUtils.hasText(fieldValue)){
			throw new Exception(fieldName + " should have proper value!!!");
		}
	}
}
