/**
 * 
 */
package com.essentials.application;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author Jay Kishore
 *
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.essentials")
public class AppConfig {

}
