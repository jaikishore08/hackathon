package com.essentials.response.bean;

public class UnPinBean {

    private String id;
	
    private UserBean unPinBy;
    
    private String essentialId;
	
	private boolean monday=false;
	
	
	private boolean tuesday=false;
	
	
	private boolean wedenesday=false;
	
	
	private boolean thrusday=false;
	
	
	private boolean friday=false;
	
	
	private boolean saturday=false;
	
	
	private boolean sunday=false;
	
	
	private boolean morning=false;
	
	
	private boolean evening=false;
	
	
	private boolean night=false;



	

	/**
	 * @return the monday
	 */
	public boolean isMonday() {
		return monday;
	}


	/**
	 * @param monday the monday to set
	 */
	public void setMonday(boolean monday) {
		this.monday = monday;
	}


	/**
	 * @return the tuesday
	 */
	public boolean isTuesday() {
		return tuesday;
	}


	/**
	 * @param tuesday the tuesday to set
	 */
	public void setTuesday(boolean tuesday) {
		this.tuesday = tuesday;
	}


	/**
	 * @return the wedenesday
	 */
	public boolean isWedenesday() {
		return wedenesday;
	}


	/**
	 * @param wedenesday the wedenesday to set
	 */
	public void setWedenesday(boolean wedenesday) {
		this.wedenesday = wedenesday;
	}


	/**
	 * @return the thrusday
	 */
	public boolean isThrusday() {
		return thrusday;
	}


	/**
	 * @param thrusday the thrusday to set
	 */
	public void setThrusday(boolean thrusday) {
		this.thrusday = thrusday;
	}


	/**
	 * @return the friday
	 */
	public boolean isFriday() {
		return friday;
	}


	/**
	 * @param friday the friday to set
	 */
	public void setFriday(boolean friday) {
		this.friday = friday;
	}


	/**
	 * @return the saturday
	 */
	public boolean isSaturday() {
		return saturday;
	}


	/**
	 * @param saturday the saturday to set
	 */
	public void setSaturday(boolean saturday) {
		this.saturday = saturday;
	}


	/**
	 * @return the sunday
	 */
	public boolean isSunday() {
		return sunday;
	}


	/**
	 * @param sunday the sunday to set
	 */
	public void setSunday(boolean sunday) {
		this.sunday = sunday;
	}


	/**
	 * @return the morning
	 */
	public boolean isMorning() {
		return morning;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}


	

	/**
	 * @return the unPinBy
	 */
	public UserBean getUnPinBy() {
		return unPinBy;
	}


	/**
	 * @param unPinBy the unPinBy to set
	 */
	public void setUnPinBy(UserBean unPinBy) {
		this.unPinBy = unPinBy;
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * @return the essentialId
	 */
	public String getEssentialId() {
		return essentialId;
	}


	/**
	 * @param essentialId the essentialId to set
	 */
	public void setEssentialId(String essentialId) {
		this.essentialId = essentialId;
	}


	/**
	 * @param morning the morning to set
	 */
	public void setMorning(boolean morning) {
		this.morning = morning;
	}


	/**
	 * @return the evening
	 */
	public boolean isEvening() {
		return evening;
	}


	/**
	 * @param evening the evening to set
	 */
	public void setEvening(boolean evening) {
		this.evening = evening;
	}


	/**
	 * @return the night
	 */
	public boolean isNight() {
		return night;
	}


	/**
	 * @param night the night to set
	 */
	public void setNight(boolean night) {
		this.night = night;
	}
	
	


}
