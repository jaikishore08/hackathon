/**
 * 
 */
package com.essentials.response.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JKishore
 *
 */
public class EssentialsBean {
	
	private String Id;
	
	private String essentialName;
	
	private String keywords;
	
	private String contact;

	private String categoryId;

	private String subCategoryId;

	private String lattitude;

	private String longitude;

	private String address;

	private List<PinBean> pinList;

	private List<UnPinBean> unPinList;

	private List<ReviewBean> reviewList;

	/**
	 * @return the id
	 */
	public String getId() {
		return Id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		Id = id;
	}

	/**
	 * @return the essentialName
	 */
	public String getEssentialName() {
		return essentialName;
	}

	/**
	 * @param essentialName the essentialName to set
	 */
	public void setEssentialName(String essentialName) {
		this.essentialName = essentialName;
	}

	/**
	 * @return the keywords
	 */
	public String getKeywords() {
		return keywords;
	}

	/**
	 * @param keywords the keywords to set
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	
	/**
	 * @return the lattitude
	 */
	public String getLattitude() {
		return lattitude;
	}

	/**
	 * @param lattitude the lattitude to set
	 */
	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the pinList
	 */
	public List<PinBean> getPinList() {
		if (this.pinList == null) {
			this.pinList= new ArrayList<PinBean>();
		}
		return pinList;
	}

	/**
	 * @param pinList the pinList to set
	 */
	public void setPinList(List<PinBean> pinList) {
		this.pinList = pinList;
	}

	/**
	 * @return the unPinList
	 */
	public List<UnPinBean> getUnPinList() {
		if (this.unPinList == null) {
			this.unPinList= new ArrayList<UnPinBean>();
		}
		return unPinList;
	}

	/**
	 * @param unPinList the unPinList to set
	 */
	public void setUnPinList(List<UnPinBean> unPinList) {
		this.unPinList = unPinList;
	}

	/**
	 * @return the reviewList
	 */
	public List<ReviewBean> getReviewList() {
		if (this.reviewList == null) {
			this.reviewList = new ArrayList<ReviewBean>();
		}
		return reviewList;
	}

	/**
	 * @param reviewList the reviewList to set
	 */
	public void setReviewList(List<ReviewBean> reviewList) {
		this.reviewList = reviewList;
	}

	/**
	 * @return the categoryId
	 */
	public String getCategoryId() {
		return categoryId;
	}

	/**
	 * @param categoryId the categoryId to set
	 */
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	/**
	 * @return the subCategoryId
	 */
	public String getSubCategoryId() {
		return subCategoryId;
	}

	/**
	 * @return the contact
	 */
	public synchronized String getContact() {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public synchronized void setContact(String contact) {
		this.contact = contact;
	}

	/**
	 * @param subCategoryId the subCategoryId to set
	 */
	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	
	

}
