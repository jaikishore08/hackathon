/**
 * 
 */
package com.essentials.response.bean;

/**
 * @author JKishore
 *
 */
public class ReviewBean {
	private String id;
	private UserBean reviewBy;
	private String essentialId;
	private int reviewCount;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
		
	/**
	 * @return the reviewCount
	 */
	public int getReviewCount() {
		return reviewCount;
	}
	/**
	 * @param reviewCount the reviewCount to set
	 */
	public void setReviewCount(int reviewCount) {
		this.reviewCount = reviewCount;
	}
	/**
	 * @return the essentialId
	 */
	public String getEssentialId() {
		return essentialId;
	}
	/**
	 * @param essentialId the essentialId to set
	 */
	public void setEssentialId(String essentialId) {
		this.essentialId = essentialId;
	}
	/**
	 * @param reviewBy the reviewBy to set
	 */
	public void setReviewBy(UserBean reviewBy) {
		this.reviewBy = reviewBy;
	}
	/**
	 * @return the reviewBy
	 */
	public UserBean getReviewBy() {
		return reviewBy;
	}
	
	
}
