package com.essentials.service;

import org.springframework.stereotype.Component;

@Component
public class EssentialHelper {
	public static String DELIMTER = ",";
	
	public String generateKeyword(String categoryName, String subCategoryName, String essentialName){
		return categoryName + DELIMTER + subCategoryName + DELIMTER + essentialName;
	}

}
