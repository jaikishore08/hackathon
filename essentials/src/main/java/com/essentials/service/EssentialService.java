package com.essentials.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.essentials.dao.IEssentialDao;
import com.essentials.model.Category;
import com.essentials.model.Essential;
import com.essentials.model.SubCategory;
import com.essentials.model.User;
import com.essentials.requestresponse.AddEssentialsRequest;
import com.essentials.requestresponse.AddUserRequest;
import com.essentials.response.bean.UserBean;
import com.essentials.util.EssentialUtil;

@Service
public class EssentialService implements IEssentialService {
	@Autowired
	private IEssentialDao essentialDao;

	@Autowired
	private EssentialHelper essentialHelper;

	public Map<String, List<String>> getAllEssentialsCountByLocation(AddEssentialsRequest addEssentialsRequest) throws Exception {
		List<Essential> essentialList = essentialDao.getAllEssentialsCountByLocation(addEssentialsRequest.getLatitude(), addEssentialsRequest.getLongitude());
		Map<String, List<String>> categoryCountMap = new HashMap<String, List<String>>();
		for(Essential essential: essentialList){
			if(categoryCountMap.keySet().contains(essential.getCategory().getId())){
				List<String> values = categoryCountMap.get(essential.getCategory().getId());
				int count = Integer.parseInt(values.get(1));
				
				List<String> newValues = new ArrayList<String>();
				newValues.add(values.get(0));
				newValues.add(""+ (++count));
				categoryCountMap.put(essential.getCategory().getId(), newValues);
			}else{
				List<String> values = new ArrayList<String>();
				Category category = essential.getCategory();
				values.add(category.getCategoryName());
				values.add("1");
				categoryCountMap.put(essential.getCategory().getId(), values);
			}
		}
		
		return categoryCountMap;
	}

	public List<Essential> getEssentialsByCategory(AddEssentialsRequest addEssentialsRequest)
			throws Exception {
		return essentialDao.getEssentialsByCategory(addEssentialsRequest.getLatitude(), addEssentialsRequest.getLongitude(), addEssentialsRequest.getCategoryId());
	}

	@Transactional
	public void addEssential(AddEssentialsRequest addEssentialsRequest) throws Exception {
		Category category = essentialDao.getCategoryById(addEssentialsRequest.getCategoryId());
		SubCategory subCategory = null;
		String subCategoryName = "";
		if(addEssentialsRequest.getSubCategoryId() != null){
			subCategory = essentialDao.getSubCategoryById(addEssentialsRequest.getSubCategoryId());
			if(subCategory != null){
				subCategoryName = subCategory.getSubCategoryName();
			}
		}
		String keywords = essentialHelper.generateKeyword(category.getCategoryName(), subCategoryName,
				addEssentialsRequest.getEssentialName());
		essentialDao.addEssential(addEssentialsRequest.getUserId(),addEssentialsRequest.getContact(), addEssentialsRequest.getLatitude(), addEssentialsRequest.getLongitude(), addEssentialsRequest.getAddress(), category, subCategory, getRewardPoints(addEssentialsRequest),
				addEssentialsRequest.getEssentialName(), keywords);
	}

	@Transactional
	public void unpinEssential(AddEssentialsRequest addEssentialsRequest) throws Exception {
		essentialDao.unpinEssential(addEssentialsRequest.getEssentialId(), addEssentialsRequest.getUserId(), getRewardPoints(addEssentialsRequest));
	}

	@Transactional
	public void verifyEssential(AddEssentialsRequest addEssentialsRequest) throws Exception {
		essentialDao.verifyEssential(addEssentialsRequest.getEssentialId(), addEssentialsRequest.getUserId(), getRewardPoints(addEssentialsRequest));
	}

	private int getRewardPoints(AddEssentialsRequest addEssentialsRequest) {
		int rewardPoints = 0;
		if(addEssentialsRequest.getRating() != null){
			rewardPoints = Integer.parseInt(addEssentialsRequest.getRating());
		}
		return rewardPoints;
	}

	@Transactional
	public void registerUser(AddUserRequest addUserRequest) throws Exception{
		EssentialUtil.validateForNullAndEmpty("User Name", addUserRequest.getName());
		EssentialUtil.validateForNullAndEmpty("Password",addUserRequest.getPassword());
		EssentialUtil.validateForNullAndEmpty("EMail Address",addUserRequest.getEmail());
		essentialDao.addUser(addUserRequest.getName(), addUserRequest.getPassword(), addUserRequest.getEmail());
	}

	public UserBean loginUser(AddUserRequest addUserRequest) throws Exception{
		User user = essentialDao.getUserByEmail(addUserRequest.getEmail());
		UserBean bean=null;
		if(user != null){
			if(addUserRequest.getPassword().equals(addUserRequest.getPassword())){
				BigInteger rewardCount = BigInteger.ZERO;
				bean=user.buildUserBean();
				/*try{
					rewardCount = essentialDao.getUserRewardPoints(user);
				}catch(Exception exe){
					System.out.println("Exception while fetching reward points");
				}
				
				userMap = new HashMap<User,BigInteger>();
				userMap.put(user, rewardCount);*/				
			}
		}else{
			throw new Exception("User does not exist!!");
		}
		
		return bean;
	}

	public List<Essential> getEssentialsByCategoryWithinRange(AddEssentialsRequest addEssentialsRequest)
			throws Exception {
		float rangeRadiusInKM = 10.0f;
		if(StringUtils.hasText(addEssentialsRequest.getRange())){
			float temp = Float.parseFloat(addEssentialsRequest.getRange());
			rangeRadiusInKM = temp/1000;
		}
		return essentialDao.getEssentialsByCategoryWithinRange(addEssentialsRequest.getLatitude(), addEssentialsRequest.getLongitude(), addEssentialsRequest.getCategoryId(), rangeRadiusInKM);
	}

	public List<Essential> getEssentialsByKeyword(
			AddEssentialsRequest addEssentialsRequest) throws Exception {
		if(addEssentialsRequest.getKeyword() != null){
			return essentialDao.getEssentialsByKeyword(addEssentialsRequest.getKeyword());
		}
		return null;
	}

	public List<Category> getAllCategories() throws Exception {
		return essentialDao.getAllCategory();
	}

	@Override
	public List<SubCategory> getAllSubCategories() throws Exception {
		return essentialDao.getAllSubCategory();
	}
}
