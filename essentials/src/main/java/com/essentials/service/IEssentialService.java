package com.essentials.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import com.essentials.model.Category;
import com.essentials.model.Essential;
import com.essentials.model.SubCategory;
import com.essentials.model.User;
import com.essentials.requestresponse.AddEssentialsRequest;
import com.essentials.requestresponse.AddUserRequest;
import com.essentials.response.bean.UserBean;

public interface IEssentialService {
	public Map<String, List<String>> getAllEssentialsCountByLocation(AddEssentialsRequest addEssentialsRequest) throws Exception;
	public List<Essential> getEssentialsByCategory(AddEssentialsRequest addEssentialsRequest) throws Exception;
	public void addEssential(AddEssentialsRequest addEssentialsRequest) throws Exception;
	public void verifyEssential(AddEssentialsRequest addEssentialsRequest) throws Exception;
	public void unpinEssential(AddEssentialsRequest addEssentialsRequest) throws Exception;
	
	public void registerUser(AddUserRequest addUserRequest) throws Exception;
	public UserBean loginUser(AddUserRequest addUserRequest) throws Exception;
	public List<Essential> getEssentialsByCategoryWithinRange(AddEssentialsRequest addEssentialsRequest) throws Exception;
	
	public List<Essential> getEssentialsByKeyword(AddEssentialsRequest addEssentialsRequest) throws Exception;
	public List<Category> getAllCategories() throws Exception;
	public List<SubCategory> getAllSubCategories() throws Exception;
}
