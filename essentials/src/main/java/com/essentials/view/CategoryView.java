package com.essentials.view;

import java.io.Serializable;
import java.util.List;

import com.essentials.model.SubCategory;

public class CategoryView implements Serializable {
		
		private static final long serialVersionUID = 1L;	
	    
		private String categoryName;
		
		private String id;
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		private List<SubCategoryView> subCategoryList;

		public List<SubCategoryView> getSubCategoryList() {
			return subCategoryList;
		}


		public void setSubCategoryList(List<SubCategoryView> subCategoryList) {
			this.subCategoryList = subCategoryList;
		}


		public String getCategoryName() {
			return categoryName;
		}


		public void setCategoryName(String categoryName) {
			this.categoryName = categoryName;
		}


}
