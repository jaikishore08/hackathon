package com.essentials.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;


@Entity
public class Reward extends Root implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@OneToOne
	@JoinColumn(name = "user")
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "Essential_Id")
	private Essential essential;
	
	private int rewardPoint;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Essential getEssential() {
		return essential;
	}

	public void setEssential(Essential essential) {
		this.essential = essential;
	}

	public int getRewardPoint() {
		return rewardPoint;
	}

	public void setRewardPoint(int rewardPoint) {
		this.rewardPoint = rewardPoint;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
