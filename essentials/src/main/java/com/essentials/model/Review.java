package com.essentials.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.essentials.response.bean.ReviewBean;

@Entity
public class Review extends Root implements Serializable {
	
	@OneToOne
	@JoinColumn(name = "reviewby")
	private User reviewBy;
	
	@OneToOne
	@JoinColumn( name = "essential")
	private Essential essential;
	
	private int reviewCount;

	public User getReviewBy() {
		return reviewBy;
	}

	public void setReviewBy(User reviewBy) {
		this.reviewBy = reviewBy;
	}

	public Essential getEssential() {
		return essential;
	}

	public void setEssential(Essential essential) {
		this.essential = essential;
	}

	public int getReviewCount() {
		return reviewCount;
	}

	public void setReviewCount(int reviewCount) {
		this.reviewCount = reviewCount;
	}
	
	
	public ReviewBean buildReviewBean(){
		ReviewBean bean=new ReviewBean();
		bean.setId(this.getId());
		bean.setEssentialId(this.getEssential().getId());
		bean.setReviewCount(this.getReviewCount());
		bean.setReviewBy(this.getReviewBy().buildUserBean());
		return bean;
	}
	

}
