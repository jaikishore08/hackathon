package com.essentials.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.essentials.response.bean.EssentialsBean;

@Entity
public class Essential extends Root implements Serializable {

	private static final long serialVersionUID = 1L;

	private String essentialName;
	
	private String keywords;
	
	private String contact;

	@OneToOne
	@JoinColumn(name = "category_id")
	private Category category;

	@OneToOne
	@JoinColumn(name = "subcategory_id")
	private SubCategory subCategory;

	private String lattitude;

	private String longitude;

	private String address;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "essential")
	private List<Pin> pinList;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "essential")
	private List<UnPin> unPinList;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "essential")
	private List<Review> reviewList;

	public String getEssentialName() {
		return essentialName;
	}

	public void setEssentialName(String essentialName) {
		this.essentialName = essentialName;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public SubCategory getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(SubCategory subCategory) {
		this.subCategory = subCategory;
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Pin> getPinList() {
		if (this.pinList == null) {
			this.pinList= new ArrayList<Pin>();
		}

		return this.pinList;
	}

	public void setPinList(List<Pin> pinList) {
		this.pinList = pinList;
	}

	public List<UnPin> getUnPinList() {
		if (this.unPinList == null) {
			this.unPinList= new ArrayList<UnPin>();
		}

		return this.unPinList;

	}

	public void setUnPinList(List<UnPin> unPinList) {
		this.unPinList = unPinList;
	}

	public List<Review> getReviewList() {
		if (this.reviewList == null) {
			this.reviewList= new ArrayList<Review>();
		}

		return this.reviewList;
	}

	public void setReviewList(List<Review> reviewList) {
		this.reviewList = reviewList;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	
	public EssentialsBean buildEssentialsBean() {
		EssentialsBean essentialsBean = new EssentialsBean();
		essentialsBean.setId(this.getId());
		essentialsBean.setEssentialName(this.getEssentialName());
		essentialsBean.setKeywords(this.getKeywords());
		essentialsBean.setCategoryId(this.getCategory().getId());
		essentialsBean.setContact(this.getContact());
		if(this.getSubCategory()!=null){
			essentialsBean.setSubCategoryId(this.getSubCategory().getId());
		}
		essentialsBean.setLattitude(this.getLattitude());
		essentialsBean.setLongitude(this.getLongitude());
		essentialsBean.setAddress(this.getAddress());
		
		for(Pin pin : this.getPinList()){
			essentialsBean.getPinList().add(pin.buildPinBean());	
		}
		
		for(UnPin unPin : this.getUnPinList()){
			essentialsBean.getUnPinList().add(unPin.buildUnPinBean());	
		}
		
		for(Review review : this.getReviewList()){
			essentialsBean.getReviewList().add(review.buildReviewBean());	
		}
		
		return essentialsBean;
	}

	/**
	 * @return the contact
	 */
	public synchronized String getContact() {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public synchronized void setContact(String contact) {
		this.contact = contact;
	}

}
