package com.essentials.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

import com.essentials.response.bean.UserBean;


@Entity
@Table(name = "User",uniqueConstraints = {@UniqueConstraint(columnNames = {"email"})})
public class User extends Root implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	
	@Email
	@NotNull
	private String email;
	
	@NotNull
	private String password;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public UserBean buildUserBean(){
		UserBean bean=new UserBean();
		bean.setId(this.getId());
		bean.setName(this.getName());
		bean.setEmail(this.getEmail());
		
		return bean;
	}
	

	
}
