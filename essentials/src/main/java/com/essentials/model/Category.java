package com.essentials.model;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;



@Entity
@Table(name = "EssentialsCategory")
public class Category extends Root implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
    
	@Column(name = "CategoryName")
	private String categoryName;

	
	@OneToMany(cascade = CascadeType.ALL,  fetch = FetchType.EAGER, mappedBy = "category")
	private List<SubCategory> subCategoryList;

	public List<SubCategory> getSubCategoryList() {
		if (this.subCategoryList == null) {
			this.subCategoryList= new ArrayList<SubCategory>();
		}
		return subCategoryList;
	}


	public void setSubCategoryList(List<SubCategory> subCategoryList) {
		this.subCategoryList = subCategoryList;
	}


	public String getCategoryName() {
		return categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	
	
	

}
