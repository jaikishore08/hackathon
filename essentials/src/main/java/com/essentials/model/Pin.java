package com.essentials.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Type;

import com.essentials.response.bean.PinBean;

@Entity
public class Pin extends Root implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@OneToOne
	@JoinColumn(name = "pinby")	
	private User pinBy;
	
	@ManyToOne
	@JoinColumn(name = "Essential_Id")
	private Essential essential;
	
	@Type(type = "numeric_boolean")
	private boolean monday=false;
	
	@Type(type = "numeric_boolean")
	private boolean tuesday=false;
	
	@Type(type = "numeric_boolean")
	private boolean wedenesday=false;
	
	@Type(type = "numeric_boolean")
	private boolean thrusday=false;
	
	@Type(type = "numeric_boolean")
	private boolean friday=false;
	
	@Type(type = "numeric_boolean")
	private boolean saturday=false;
	
	@Type(type = "numeric_boolean")
	private boolean sunday=false;
	
	@Type(type = "numeric_boolean")
	private boolean morning=false;
	
	@Type(type = "numeric_boolean")
	private boolean evening=false;
	
	@Type(type = "numeric_boolean")
	private boolean night=false;
	
	public User getPinBy() {
		return pinBy;
	}
	public void setPinBy(User pinBy) {
		this.pinBy = pinBy;
	}
	public Essential getEssential() {
		return essential;
	}
	public void setEssential(Essential essential) {
		this.essential = essential;
	}
	public boolean isMonday() {
		return monday;
	}
	public void setMonday(boolean monday) {
		this.monday = monday;
	}
	public boolean isTuesday() {
		return tuesday;
	}
	public void setTuesday(boolean tuesday) {
		this.tuesday = tuesday;
	}
	public boolean isWedenesday() {
		return wedenesday;
	}
	public void setWedenesday(boolean wedenesday) {
		this.wedenesday = wedenesday;
	}
	public boolean isThrusday() {
		return thrusday;
	}
	public void setThrusday(boolean thrusday) {
		this.thrusday = thrusday;
	}
	public boolean isFriday() {
		return friday;
	}
	public void setFriday(boolean friday) {
		this.friday = friday;
	}
	public boolean isSaturday() {
		return saturday;
	}
	public void setSaturday(boolean saturday) {
		this.saturday = saturday;
	}
	public boolean isSunday() {
		return sunday;
	}
	public void setSunday(boolean sunday) {
		this.sunday = sunday;
	}
	public boolean isMorning() {
		return morning;
	}
	public void setMorning(boolean morning) {
		this.morning = morning;
	}
	public boolean isEvening() {
		return evening;
	}
	public void setEvening(boolean evening) {
		this.evening = evening;
	}
	public boolean isNight() {
		return night;
	}
	public void setNight(boolean night) {
		this.night = night;
	}	
	
	public PinBean buildPinBean() {
		PinBean pinBean=new PinBean();
		pinBean.setId(this.getId());
		pinBean.setEssentialId(this.getEssential().getId());
		pinBean.setPinBy(this.getPinBy().buildUserBean());
		pinBean.setMonday(this.isMonday());
		pinBean.setTuesday(this.isTuesday());
		pinBean.setWedenesday(this.isWedenesday());
		pinBean.setThrusday(this.isThrusday());
		pinBean.setFriday(this.isFriday());
		pinBean.setSaturday(this.isSaturday());
		pinBean.setSunday(this.isSunday());
		pinBean.setMorning(this.isMorning());
		pinBean.setEvening(this.isEvening());
		pinBean.setNight(this.isNight());
		return pinBean;
		
	}
}
