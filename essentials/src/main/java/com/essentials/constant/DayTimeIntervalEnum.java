package com.essentials.constant;

public enum DayTimeIntervalEnum {
	MORNING("Morning"),
	AFTERNOON("Afternoon"),
	EVEVING("Evening"),
	NIGHT("Night");
	
	private final String dayTimeIntervalEnumValue;
	
	DayTimeIntervalEnum(String dayTime){
		dayTimeIntervalEnumValue = dayTime;
	}
	
	public String value() {
		return dayTimeIntervalEnumValue;
	}

	public static DayTimeIntervalEnum fromValue(String v) {
		for (DayTimeIntervalEnum dayTimeIntervalEnum : DayTimeIntervalEnum.values()) {
			if (dayTimeIntervalEnum.dayTimeIntervalEnumValue.equalsIgnoreCase(v)) {
				return dayTimeIntervalEnum;
			}
		}
		throw new IllegalArgumentException(String.valueOf(v));
	}	
	
}
