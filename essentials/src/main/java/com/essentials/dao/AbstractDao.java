/**
 * 
 */
package com.essentials.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author AKumar4
 *
 */
public class AbstractDao {
	   
	    @Autowired
	    private SessionFactory sessionFactory;
	    
	    
	    private final Class persistentClass = null;
	    
	 
	    protected Session getSession() {
	        return sessionFactory.getCurrentSession();
	    }
	 
	    public void persist(Object entity) {
	        getSession().persist(entity);
	    }
	    
	    
	    public void saveOrUpdate(Object entity) {
	    	getSession().saveOrUpdate(entity);
	    }
	    
	    public void merge(Object entity) {
	    	getSession().merge(entity);
	    }
	    
	    public void delete(Object entity) {
	        getSession().delete(entity);
	    }
	    
	    public Object save(Object entity) {
	    	return getSession().save(entity);
	    }
	    
	    
	    
	    public int getByKey(int key) {
	        return  (Integer) getSession().get(persistentClass, key);
	    }

	    
	    
	    //For reference only
	  /*  @SuppressWarnings("unchecked")
	    public List<Employee> findAllEmployees() {
	        Criteria criteria = getSession().createCriteria(Employee.class);
	        return (List<Employee>) criteria.list();
	    }
	 
	    public void deleteEmployeeBySsn(String ssn) {
	        Query query = getSession().createSQLQuery("delete from Employee where ssn = :ssn");
	        query.setString("ssn", ssn);
	        query.executeUpdate();
	    }
	 
	     
	    public Employee findBySsn(String ssn){
	        Criteria criteria = getSession().createCriteria(Employee.class);
	        criteria.add(Restrictions.eq("ssn",ssn));
	        return (Employee) criteria.uniqueResult();
	    }*/
}
