package com.essentials.dao;

import java.math.BigInteger;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import com.essentials.model.Category;
import com.essentials.model.Essential;
import com.essentials.model.Pin;
import com.essentials.model.Review;
import com.essentials.model.SubCategory;
import com.essentials.model.UnPin;
import com.essentials.model.User;
import com.essentials.util.EssentialUtil;


@Repository
@Transactional
public  class EssentialDao extends AbstractDao implements IEssentialDao {
	
//   	@Autowired
//	AbstractDao abstractDao;


	public List<Essential> getAllEssentialsCountByLocation(String latitude, String longitude) throws Exception {
		float rangeRadiusInKM = 5;
		String query = "from Essential where SQRT( POW(69.1 * (lattitude - " + latitude +"), 2) + POW(69.1 * ( "+
        		longitude +" - longitude) * COS(lattitude / 57.3), 2)) < " + rangeRadiusInKM +"";
		        
		Query hqlQuery = getSession().createQuery(query);
        List<Essential> essentialList = hqlQuery.list();
		return essentialList;
	}

	public List<Essential> getEssentialsByCategory(String latitude, String longitude, String categoryId)
			throws Exception {
		float rangeRadiusInKM = 5;
		String query = "from Essential where SQRT( POW(69.1 * (lattitude - " + latitude +"), 2) + POW(69.1 * ( "+
        		longitude +" - longitude) * COS(lattitude / 57.3), 2)) < " + rangeRadiusInKM +" and category="+categoryId+"";
		        
		Query hqlQuery = getSession().createQuery(query);
        List<Essential> essentialList = hqlQuery.list();
		return essentialList;
	}
		
		

	public void verifyEssential(String essentialId, String userId, int reviewCount) throws Exception{
		// TODO Auto-generated method stub
		Pin pin = new Pin();
	    EssentialUtil.setDayForPin(pin);
	    EssentialUtil.setDayTimeForPin(pin);
	    
	    //get essential from session
	    Essential essential = (Essential) getSession().get(Essential.class, essentialId);
	    pin.setEssential(essential);
	    
	    //get user by userId
	    User user = (User) getSession().get(User.class,userId);
	    pin.setPinBy(user);
	    
	    String pinId = (String) save(pin);
	    pin.setId(pinId);
	    essential.getPinList().add(pin);
	    
	    Review review =new Review();
		review.setEssential(essential);
		review.setReviewBy(user);
		review.setReviewCount(reviewCount);
		String reviewId = (String) save(review);
		review.setId(reviewId);
		essential.getReviewList().add(review);
		
	}

	public void unpinEssential(String essentialId, String userId, int rewardPoints) throws Exception{
		
		UnPin unpin = new UnPin();
	    EssentialUtil.setDayForUnPin(unpin);
	    EssentialUtil.setDayTimeForUnPin(unpin);
	    
	    //get essential from session
	    Essential essential = (Essential) getSession().get(Essential.class, essentialId);
	    unpin.setEssential(essential);
	    
	    //get user by userId
	    User user = (User) getSession().get(User.class,userId);
	    unpin.setUnPinBy(user);
	    
	    String unpinId = (String) save(unpin);
	    unpin.setId(unpinId);
	    essential.getUnPinList().add(unpin);
	}

	
	
	
	public SubCategory getSubCategoryById(String subCategoryId)
			throws Exception {
	
        Query query = getSession().createQuery("FROM SubCategory where PRIMARYID = :subCategoryId");
		
		query.setString("subCategoryId", subCategoryId);
		SubCategory subCategory = (SubCategory) query.uniqueResult();
		
		return  subCategory;
		                  
		
		
	}

	public void addEssential(String userId,String contact ,String latitude, String longitude, String address, Category category,
			SubCategory subCategory, int reviewCount, String essentialName, String keywords) throws Exception {
		Essential essential = new Essential();
		essential.setEssentialName(essentialName);
		essential.setLongitude(longitude);
		essential.setLattitude(latitude);
		essential.setAddress(address);
		essential.setKeywords(keywords);
		essential.setCategory(category);
		essential.setSubCategory(subCategory);
		essential.setContact(contact);
		
	    String essentialId = (String) save(essential);
	    essential.setId(essentialId);
        
	   
		//Call EssentialUtil method to set time of the day and day of week	
	    Pin pin = new Pin();
	    EssentialUtil.setDayForPin(pin);
	    EssentialUtil.setDayTimeForPin(pin);
	    
	    User user = (User) getSession().get(User.class,userId);
	    pin.setPinBy(user);
	    
		pin.setEssential(essential);
		String pinId = (String) save(pin);
		pin.setId(pinId);
		
		essential.getPinList().add(pin);
		
		Review review =new Review();
		review.setEssential(essential);
		review.setReviewBy(user);
		review.setReviewCount(reviewCount);
		String reviewId = (String) save(review);
		review.setId(reviewId);
		essential.getReviewList().add(review);
	}

	public List<Essential> getEssentialsByCategoryWithinRange(String latitude, String longitude, String categoryId,
			Float rangeRadiusInKM) throws Exception {
		
		String query = "from Essential where SQRT( POW(69.1 * (lattitude - " + latitude +"), 2) + POW(69.1 * ( "+
        		longitude +" - longitude) * COS(lattitude / 57.3), 2)) < " + rangeRadiusInKM +" and category="+categoryId+"";
	        
		Query hqlQuery = getSession().createQuery(query);
		List<Essential> essentialWithinRange = hqlQuery.list();
		
		return essentialWithinRange;
	}

	public User getUserByName(String userName)  throws Exception{
		User user = new User();
		Query query=getSession().createQuery("from User as u WHERE name = :userName");
		query.setString("userName", userName);
		List<User> userList = query.list();  
		return userList.get(0);
	}

	public User addUser(String userName, String password, String emailId) throws Exception{
				
		User user = new User();
		user.setName(userName);
		user.setPassword(password);
		user.setEmail(emailId);
		String userId = (String) save(user);
		user.setId(userId);
		return user;

	}

	
	public Category getCategoryById(String categoryId) throws Exception{
		// TODO Auto-generated method stub
		
		Query query = getSession().createQuery("from Category where id = :category_id");
		query.setString("category_id", categoryId);
		List<Category> categoryList = query.list();

				
		return categoryList.get(0);
	}

	public User getUserByEmail(String email) throws Exception{
		User user = new User();
		Query query=getSession().createQuery("from User as u WHERE email = :email");
		query.setString("email", email);
		List<User> userList = query.list();  
		return userList.get(0);
	}

	public BigInteger getUserRewardPoints(User user) throws Exception{
		SQLQuery query = getSession().createSQLQuery("SELECT user, sum(rewardPoint) from reward where user = :userId group by user");
		
		query.setString("userId",user.getId());
		List userRewards = query.list();
		BigInteger count = BigInteger.ZERO;
		if(userRewards != null){
			Object[]array=(Object[]) userRewards.get(0);
			count = (BigInteger)array[1];
		}
		return count;
	}

	public List<Essential> getEssentialsByKeyword(
			String keyword) throws Exception {
		Query query = getSession().createQuery("from Essetinal where keywords like ?");
		query.setString(1, keyword);
		List<Essential> essentialList = query.list();

				
		return essentialList;
	}

	public List<Category> getAllCategory() throws Exception {
		Query query=getSession().createQuery("from Category");		
		//Query query=getSession().createQuery("from Category as C left join C.subCategoryList");
		List<Category> categoryList = query.list();  
		return categoryList;
	}

	@Override
	public List<SubCategory> getAllSubCategory() throws Exception {
		Query query=getSession().createQuery("from SubCategory");		
		List<SubCategory> subCategoryList = query.list();  
		return subCategoryList;
	}

		
}
