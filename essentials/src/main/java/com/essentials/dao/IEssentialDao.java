package com.essentials.dao;

import java.math.BigInteger;
import java.util.List;

import com.essentials.model.Category;
import com.essentials.model.Essential;
import com.essentials.model.SubCategory;
import com.essentials.model.User;

public interface IEssentialDao {	
	public List<Essential> getAllEssentialsCountByLocation(String latitude,String longitude) throws Exception;
	public List<Essential> getEssentialsByCategory(String latitude,String longitude,String categoryId) throws Exception;
	public void addEssential(String userId,String contact,String latitude,String longitude,String address,Category category,SubCategory subCategory,int reviewCount, String essentialName, String keywords) throws Exception;
	public void verifyEssential(String essentialId,String userId,int reviewCount) throws Exception;
	public void unpinEssential(String essentialId,String userId,int reviewCount) throws Exception;
	
	public Category getCategoryById(String categoryId) throws Exception;
	public SubCategory getSubCategoryById(String subCategoryId) throws Exception;
	
	
	
	public List<Essential> getEssentialsByCategoryWithinRange(String latitude,String longitude,String categoryId, Float rangeRadiusInKM) throws Exception;
	public User getUserByName(String userName) throws Exception;
	public User getUserByEmail(String email) throws Exception;
	public User addUser(String userName,String password,String emailId) throws Exception;
	
	public BigInteger getUserRewardPoints(User user) throws Exception;
	
	public List<Essential> getEssentialsByKeyword(String keyword) throws Exception;
	
	public List<Category> getAllCategory() throws Exception;
	public List<SubCategory> getAllSubCategory() throws Exception;
}
