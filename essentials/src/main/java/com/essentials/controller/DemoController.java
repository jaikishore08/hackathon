/**
 * 
 */
package com.essentials.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.essentials.model.Category;
import com.essentials.model.Essential;
import com.essentials.model.SubCategory;
import com.essentials.model.User;
import com.essentials.requestresponse.ListEssentialResponse;
import com.essentials.requestresponse.AddEssentialsRequest;
import com.essentials.requestresponse.AddUserRequest;
import com.essentials.requestresponse.EssentialByCountServiceResponse;
import com.essentials.requestresponse.EssentialsByCountResponse;
import com.essentials.requestresponse.STATUS;
import com.essentials.requestresponse.UserResponse;
import com.essentials.response.bean.UserBean;
import com.essentials.view.CategoryView;
import com.essentials.view.SubCategoryView;

/**
 * @author Jay Kishore
 *
 */
@RestController
public class DemoController {
	@Autowired
	com.essentials.service.IEssentialService ObjIEssentialService;

	@RequestMapping(value = "/check", method = RequestMethod.GET)
	public ResponseEntity checkRestServices() {
		Map<String, List<String>> categoryCountMap = new HashMap<String, List<String>>();
		List<String> tempList = new ArrayList<String>();
		tempList.add("test");
		tempList.add("test1");
		categoryCountMap.put("2", tempList);
		
		ListEssentialResponse response = new ListEssentialResponse();
		//response.setCategoryCountMap(categoryCountMap);
		return new ResponseEntity("Hello", HttpStatus.OK);
		
	}

	@RequestMapping(value = "/getessentialsbylocation", method = RequestMethod.POST)
	public ResponseEntity getAllEssentialsCountByLocation(@RequestBody AddEssentialsRequest addEssentialsRequest) {
		EssentialsByCountResponse finalResponse=new EssentialsByCountResponse();
		List<EssentialByCountServiceResponse>serviceResponseList=new ArrayList<>();
		try {
			Map<String, List<String>> categoryCountMap = ObjIEssentialService
					.getAllEssentialsCountByLocation(addEssentialsRequest);
			
			for(Map.Entry<String, List<String>>entry:categoryCountMap.entrySet()){
				EssentialByCountServiceResponse response=new EssentialByCountServiceResponse();
				response.setCategoryId(entry.getKey());
				response.setCategoryName(entry.getValue().get(0));
				response.setCount(entry.getValue().get(1));
				serviceResponseList.add(response);
			}
			finalResponse.setStatus(STATUS.SUCCESS);
			finalResponse.setMessage("Data retrived successfully");
			finalResponse.setResponseList(serviceResponseList);
		} catch (Exception e) {
			e.printStackTrace();
			finalResponse.setStatus(STATUS.FAILURE);
			finalResponse.setMessage("Failed");
		}
		return new ResponseEntity(finalResponse, HttpStatus.OK);
	}

	@RequestMapping(value = "/getessentialsbycategory", method = RequestMethod.POST)
	public ResponseEntity getEssentialsByCategory(@RequestBody AddEssentialsRequest addEssentialsRequest) {
		ListEssentialResponse response = new ListEssentialResponse();
		try {
			List<Essential> requestData = ObjIEssentialService.getEssentialsByCategory(addEssentialsRequest);
			response.setStatus(STATUS.SUCCESS);
			response.setMessage(requestData.toString());
			for (Essential essential : requestData) {
				response.getEssentialsList().add(essential.buildEssentialsBean());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(STATUS.FAILURE);
			response.setMessage("Failed");
		}
		return new ResponseEntity(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/getessentialswithinrange", method = RequestMethod.POST)
	public ResponseEntity getEssentialsByCategoryWithinRange(@RequestBody AddEssentialsRequest addEssentialsRequest) {
		ListEssentialResponse response = new ListEssentialResponse();
		try {
			List<Essential> requestedData = ObjIEssentialService
					.getEssentialsByCategoryWithinRange(addEssentialsRequest);
			response.setStatus(STATUS.SUCCESS);
			response.setMessage("Data retrieved successfully");
			for (Essential essential : requestedData) {
				response.getEssentialsList().add(essential.buildEssentialsBean());
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(STATUS.FAILURE);
			response.setMessage("Failed");
		}
		return new ResponseEntity(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/addessentials", method = RequestMethod.POST, produces = { "application/json",
			"application/xml" }, consumes = "application/json")
	public ResponseEntity addEssentials(@RequestBody AddEssentialsRequest addEssentialsRequest) {
		System.out.println("addessentials");
		ListEssentialResponse response = new ListEssentialResponse();
		try {
			ObjIEssentialService.addEssential(addEssentialsRequest);
			response.setStatus(STATUS.SUCCESS);
			response.setMessage("Added Successfully");
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(STATUS.FAILURE);
			response.setMessage("Something went wrong");
		}
		return new ResponseEntity(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/verifypin", method = RequestMethod.POST)
	public ResponseEntity verifyPin(@RequestBody AddEssentialsRequest addEssentialsRequest) {
		ListEssentialResponse response = new ListEssentialResponse();
		try {
			ObjIEssentialService.verifyEssential(addEssentialsRequest);
			response.setStatus(STATUS.SUCCESS);
			response.setMessage("Pinverified Successfully");
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(STATUS.FAILURE);
			response.setMessage("Failed");
		}
		return new ResponseEntity(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/unpin", method = RequestMethod.POST)
	public ResponseEntity unPin(@RequestBody AddEssentialsRequest addEssentialsRequest) {
		ListEssentialResponse response = new ListEssentialResponse();
		try {
			ObjIEssentialService.unpinEssential(addEssentialsRequest);
			response.setStatus(STATUS.SUCCESS);
			response.setMessage("Unpined Successfully");
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(STATUS.FAILURE);
			response.setMessage("Failed");
		}
		return new ResponseEntity(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity userLogin(@RequestBody AddUserRequest addUserRequest) {
		UserResponse response = new UserResponse();

		try {
			UserBean userBean = ObjIEssentialService.loginUser(addUserRequest);
			response.setStatus(STATUS.SUCCESS);
			response.setMessage("Logged in successfully");
			response.setUser(userBean);
			
			List<Category> categoryList = ObjIEssentialService.getAllCategories();
		/*	List<Category> catList = new ArrayList<>();
			for(Object[] object: categoryList){
				Category category = (Category)object[0];
				SubCategory subCategory = (SubCategory)object[1];
				List<SubCategory> subCatgoryList = new ArrayList<>();
				subCatgoryList.add(subCategory);
				category.setSubCategoryList(subCatgoryList);
				catList.add(category);
			}*/
			List<SubCategory> subCategoryList = ObjIEssentialService.getAllSubCategories();
			
			List<CategoryView> newCategoryList = new ArrayList<CategoryView>();
			for(Category category: categoryList){
				CategoryView categoryToAdd = new CategoryView();
				categoryToAdd.setCategoryName(category.getCategoryName());
				categoryToAdd.setId(category.getId());
				//categoryToAdd.setSysCreationDate(category.getSysCreationDate());
				
				category.setSubCategoryList(null);
				categoryToAdd.setSubCategoryList(null);
				for(SubCategory subcategory:subCategoryList){
					if(subcategory.getCategory().getId().equalsIgnoreCase(category.getId())){
						List<SubCategoryView> subCategories = categoryToAdd.getSubCategoryList();
						if(subCategories != null && subCategories.size() > 0){
							SubCategoryView subCategoryView = new SubCategoryView();
							subCategoryView.setId(subcategory.getId());
							subCategoryView.setSubCategoryName(subcategory.getSubCategoryName());
							
							categoryToAdd.getSubCategoryList().add(subCategoryView);
						}else{
							subCategories = new ArrayList<SubCategoryView>();
							
							SubCategoryView subCategoryView = new SubCategoryView();
							subCategoryView.setId(subcategory.getId());
							subCategoryView.setSubCategoryName(subcategory.getSubCategoryName());
							
							subCategories.add(subCategoryView);
							categoryToAdd.setSubCategoryList(subCategories);
						}
						//categoryToAdd.setSubCategoryList(category.getSubCategoryList());						
						//category.getSubCategoryList().add(subcategory);
					}
				}
				newCategoryList.add(categoryToAdd);
			}
			response.setCategoryList(newCategoryList);
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(STATUS.FAILURE);
			response.setMessage("Login Failed");
		}
		return new ResponseEntity(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity userRegistration(@RequestBody AddUserRequest addUserRequest) {
		UserResponse response = new UserResponse();
		try {
			ObjIEssentialService.registerUser(addUserRequest);
			response.setStatus(STATUS.SUCCESS);
			response.setMessage("User Registered Successfully");
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(STATUS.FAILURE);
			response.setMessage("Registration Failed");
		}
		return new ResponseEntity(response, HttpStatus.OK);
	}
}
