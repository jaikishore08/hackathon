package com.essentials.requestresponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.essentials.model.Essential;
import com.essentials.response.bean.EssentialsBean;

public class ListEssentialResponse extends BaseResponse {
	private List<EssentialsBean> essentialsList;

	/**
	 * @return the essentialsList
	 */
	public List<EssentialsBean> getEssentialsList() {
		if(essentialsList==null){
			essentialsList=new ArrayList<>();
		}
		return essentialsList;
	}

	/**
	 * @param essentialsList the essentialsList to set
	 */
	public void setEssentialsList(List<EssentialsBean> essentialsList) {
		this.essentialsList = essentialsList;
	}
	

	
}
