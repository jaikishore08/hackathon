package com.essentials.requestresponse;

public class BaseResponse {
   private STATUS status;
   private String message;
public STATUS getStatus() {
	return status;
}
public void setStatus(STATUS status) {
	this.status = status;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
   
   
}
