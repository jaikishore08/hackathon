package com.essentials.requestresponse;

public class AddEssentialsRequest {
	private String userId,essentialId, rating, latitude, longitude, categoryId, subCategoryId, essentialName, keyword, address,range,contact;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(String subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public String getEssentialName() {
		return essentialName;
	}

	public void setEssentialName(String essentialName) {
		this.essentialName = essentialName;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEssentialId() {
		return essentialId;
	}

	public void setEssentialId(String essentialId) {
		this.essentialId = essentialId;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	/**
	 * @return the contact
	 */
	public synchronized String getContact() {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public synchronized void setContact(String contact) {
		this.contact = contact;
	}

}
