package com.essentials.requestresponse;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import com.essentials.model.Category;
import com.essentials.model.User;
import com.essentials.response.bean.UserBean;
import com.essentials.view.CategoryView;

public class UserResponse extends BaseResponse {
	private UserBean user;
	private List<CategoryView> categoryList;

	public List<CategoryView> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<CategoryView> categoryList) {
		this.categoryList = categoryList;
	}

	/**
	 * @return the user
	 */
	public synchronized UserBean getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public synchronized void setUser(UserBean user) {
		this.user = user;
	}

	
	
	
}
