/**
 * 
 */
package com.essentials.requestresponse;

import java.util.List;

/**
 * @author JKishore
 *
 */
public class EssentialsByCountResponse extends BaseResponse {
     private List<EssentialByCountServiceResponse>responseList;

	/**
	 * @return the responseList
	 */
	public List<EssentialByCountServiceResponse> getResponseList() {
		return responseList;
	}

	/**
	 * @param responseList the responseList to set
	 */
	public void setResponseList(List<EssentialByCountServiceResponse> responseList) {
		this.responseList = responseList;
	}
     
}
